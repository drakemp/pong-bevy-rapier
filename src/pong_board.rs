use bevy::prelude::*;
use bevy::sprite::MaterialMesh2dBundle;
use bevy_rapier2d::prelude::*;
use rapier2d::geometry::CollisionEventFlags;

// My defaults
const SCREEN_WIDTH: f32 = 2560.;
const SCREEN_HEIGHT: f32 = 1440.;

pub struct PongBoardPlugin;

impl Plugin for PongBoardPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, (setup_walls, add_ball));
        app.add_systems(Update, score_system);
    }
}

// START board
pub enum ScoreArea {
    Player1,
    Player2,
}

#[derive(Component)]
pub struct Score(u32);

#[derive(Component)]
pub struct Goal(ScoreArea);

pub fn setup_walls(mut commands: Commands) {
    // TOP SOLID wall
    commands.spawn((
        Collider::cuboid(SCREEN_WIDTH / 2., 1.),
        RigidBody::Fixed,
        TransformBundle::from(Transform::from_xyz(0., (SCREEN_HEIGHT) / 4., 0.)),
    ));

    // BOTTOM SOLID wall
    commands.spawn((
        Collider::cuboid(SCREEN_WIDTH / 2., 1.),
        RigidBody::Fixed,
        TransformBundle::from(Transform::from_xyz(0., -(SCREEN_HEIGHT) / 4., 0.)),
    ));

    // SCORE P1
    commands.spawn((
        Goal(ScoreArea::Player1),
        Score(0), //Goals P2 has -- I could do this better but lazy
        TransformBundle::from(Transform::from_xyz(-(SCREEN_WIDTH) / 4., 0., 0.)),
        Collider::cuboid(1., SCREEN_HEIGHT / 2.),
        Sensor,
        RigidBody::Fixed,
    ));

    // SCORE P2
    commands.spawn((
        Goal(ScoreArea::Player2),
        Score(0), //Goals P1 has
        TransformBundle::from(Transform::from_xyz((SCREEN_WIDTH) / 4., 0., 0.)),
        Collider::cuboid(1., SCREEN_HEIGHT / 2.),
        Sensor,
        RigidBody::Fixed,
    ));
}

// Collision event system looking for goals, then updating scores and positions 
pub fn score_system(
    mut collision_events: EventReader<CollisionEvent>,
    mut score_areas: Query<(&Goal, &mut Score)>,    
    mut ball: Query<(&mut Transform, &mut Velocity), With<PongBall>>,    
    ) {
    for collision_event in collision_events.iter() {
        if let CollisionEvent::Started(a, b, CollisionEventFlags::SENSOR) = collision_event {
            println!("{:?} - {:?}", a, b);
            let mut target_entity: &Entity = a; 

            // Figure out which entity was the score area
            if score_areas.contains(*b) {
                target_entity = b;
            }

            // Update Scores
            // TODO: display on screen
            if let Ok((Goal(area), mut score)) = score_areas.get_mut(*target_entity) {
                score.0 += 1;
                match area {
                    ScoreArea::Player1 => {
                        println!("Player 2 scored {:?}", score.0);
                    },
                    ScoreArea::Player2 => {
                        println!("Player 1 scored {:?}", score.0);
                    },
                }
            }
            
            // Reset ball a point directly add player who got scored on, right?
            let (mut transform, mut velocity) = ball.single_mut(); 
            transform.translation = Vec3::ZERO;
            velocity.linvel.y = 0.;
            velocity.linvel.y = 0.;
            velocity.angvel = 0.;
        }
    }
}

// END board

// START Ball

#[derive(Component)]
pub struct PongBall; //something to separate the pong ball from the rest

pub fn add_ball(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn((
        //custom
        PongBall,

        //physics
        Collider::ball(10.),                // collision area
        ColliderMassProperties::Mass(0.8),  // non-zero mass
        ActiveEvents::COLLISION_EVENTS,     // Emmit events related to the ball
                                            // this is for EventReader<> to catch
        RigidBody::Dynamic,                 // make move-able and collidable
        Velocity::default(),
        Restitution::coefficient(2.),       // bounce
        Damping {                           // Dont let ball slow down
            linear_damping: 0.,
            angular_damping: 0.,
        },
        ExternalImpulse {                   // inital push to start the game
            impulse: Vec2::new(300., 0.),
            torque_impulse: 0.,
        },

        // cosmetic
        MaterialMesh2dBundle {
            mesh: meshes.add(shape::Circle::new(10.).into()).into(),
            material: materials.add(ColorMaterial::from(Color::WHITE)),
            transform: Transform::from_translation(Vec3::new(0., 0., 0.)),
            ..default()
        },
    ));
}

// END Ball
