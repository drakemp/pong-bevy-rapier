use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

pub struct PongPlayersPlugin; //empty struct for build()

impl Plugin for PongPlayersPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, add_players);
        app.add_systems(Update, player_move);
    }
}

#[derive(Component)]
pub struct PongPlayer; // empty struct for Entity tagging sort of

#[derive(Component)]
pub struct Controls(KeyCode, KeyCode); // setup unique controls per player

// for fixing in place
const P1_X_POS: f32 = -500.;
const P2_X_POS: f32 = 500.;

// System for spawning in player entities,
//   entities are basically tuples of components
pub fn add_players(mut commands: Commands) {
    let width = 25.;
    let height = 75.;

    // Player 1
    commands.spawn((
        // Custom stuff
        PongPlayer,
        Controls(KeyCode::W, KeyCode::S), // notice controls
        // Cosmetics
        SpriteBundle {
            // basic cosmetics, no collision detection
            sprite: Sprite {
                color: Color::WHITE,
                custom_size: Some(Vec2::new(width, height)),
                ..default()
            },
            transform: Transform::from_xyz(P1_X_POS, 0., 0.),
            ..default()
        },
        // Physics | Collisions
        Collider::cuboid(width / 2., height / 2.),
        ColliderMassProperties::Mass(1.),
        RigidBody::Dynamic,
        LockedAxes::ROTATION_LOCKED
            | LockedAxes::TRANSLATION_LOCKED_X
            | LockedAxes::TRANSLATION_LOCKED_Z,
        Velocity {
            linvel: Vec2::new(0., 0.),
            angvel: 0.,
        },
        Damping {
            linear_damping: 5.,
            angular_damping: 0.,
        },
        ExternalForce::default(),
    ));

    // Player 2
    commands.spawn((
        // Custom stuff
        PongPlayer,
        Controls(KeyCode::I, KeyCode::J), // notice controls
        // Cosmetic
        SpriteBundle {
            sprite: Sprite {
                color: Color::WHITE,
                custom_size: Some(Vec2::new(width, height)),
                ..default()
            },
            transform: Transform::from_xyz(P2_X_POS, 0., 0.),
            ..default()
        },
        // Physics | Collisions
        Collider::cuboid(width / 2., height / 2.),
        ColliderMassProperties::Mass(0.8),
        RigidBody::Dynamic, //move-able and collide-able
        LockedAxes::ROTATION_LOCKED
            | LockedAxes::TRANSLATION_LOCKED_X
            | LockedAxes::TRANSLATION_LOCKED_Z,
        Velocity {
            linvel: Vec2::new(0., 0.),
            angvel: 0.,
        },
        Damping {
            linear_damping: 5.,
            angular_damping: 0.,
        },
        ExternalForce::default(),
    ));
}

// Update velocity for each player based on key presses
// Queries are fun, we can get exactly the components we need and perform the
//   same operation on all of the same type of entities. The limiting the RW
//   access to components helps speed the game up with multithreading by
//   locking on specific components rather than entire entities.
pub fn player_move(
    keys: Res<Input<KeyCode>>,
    mut players: Query<(&mut ExternalForce, &Controls), With<PongPlayer>>,
) {
    for (mut force, controls) in &mut players {
        if keys.just_pressed(controls.0) {
            force.force.y = 10_000.;
        } else if keys.just_pressed(controls.1) {
            force.force.y = -10_000.;
        } else if keys.just_released(controls.0) || keys.just_released(controls.1) {
            force.force.y = 0.;
        }
    }
}
