pub mod pong_board;
pub mod pong_players;

use pong_board::*;
use pong_players::*;

use bevy::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_rapier2d::prelude::*;

fn main() {
    App::new()
        // Basic setup
        .add_plugins(DefaultPlugins) // Window and other stuff
        .add_plugins(WorldInspectorPlugin::new()) // Debugger
        .add_systems(Startup, setup_2d) // Camera
        .insert_resource(ClearColor(Color::BLACK)) // Background

        // Physics / Collisions
        .add_plugins(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(100.))
        .add_systems(Startup, setup_physics)
        //debug physics
        .add_plugins(RapierDebugRenderPlugin::default())
        .add_systems(Update, display_events)
        
        // Custom Game stuff
        .add_plugins(PongPlayersPlugin) // Players
        .add_plugins(PongBoardPlugin) // Board
        .run(); //start game
}

// System for game camera
fn setup_2d(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

// Disable gravity (we have a topdown view)
fn setup_physics(mut config: ResMut<RapierConfiguration>) {
    config.gravity = Vect::ZERO;
}

// Debugging collisions
fn display_events(
    mut collision_events: EventReader<CollisionEvent>,
    mut contact_force_events: EventReader<ContactForceEvent>,
) {
    for collision_event in collision_events.iter() {
        println!("Received collision event: {:?}", collision_event);
    }

    for contact_force_event in contact_force_events.iter() {
        println!("Received contact force event: {:?}", contact_force_event);
    }
}
